/* This creates the tables for the bank db. */

CREATE TABLE Members (
	SSN integer,
	Name text,
        PRIMARY KEY (SSN),
);

CREATE TABLE Transactions (
	SSN integer,
	Number blob,
	Time datetime,
	Amount blob,
        PRIMARY KEY (SSN,Number,Time),
        FOREIGN KEY (SSN) REFERENCES Members(SSN)
);

CREATE TABLE Accounts (
	SSN blob,
	Number integer,
	Balance blob,
	Type text,
        PRIMARY KEY (SSN,Number),
        FOREIGN KEY (SSN) REFERENCES Members(SSN)
);


/* Insert data into tables */




