This is a project I used to help me learn alot more about C++ and writing console programs.
It was inspired by this blog post [here](https://www.codewithc.com/banking-record-system-project-c/)

Tools used in this project 
- C++ (compiled with clang)
- SQLite3
- ncurses

The current db schema looks like this...(will be updated as the project goes on)
![Screenshot](database.png)

Roadmap
- build out ui
- add statistics and visuals on spending
- makefiles and deps for launching on other systems
- docker implementation for portability